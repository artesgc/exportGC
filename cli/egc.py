from os import curdir, system, path
from platform import platform
from shutil import copytree
from subprocess import getoutput
from sys import argv, exit
from time import sleep, ctime

__updated__ = '2021-03-29 08:23:40'
__author__ = 'Nurul-GC'
__copyright__ = '(C) 2021 Nurul-GC'
__trademark__ = '(TM) ArtesGC Inc'


class EGC:
    """
    * UMA SOLUÇAO PARA EXPORTAÇAO DOS MODULOS DO NODEJS OFFLINE
    * (C) 2021 Nurul-GC - (TM) ArtesGC
    """

    def __init__(self):
        self.sistema_operativo = platform()
        self.moduloExportar = None
        self.nomeUsuario = None
        self.dirProjecto = None

        self.DATAHORA = ctime()

        # ***** CORES-TERMINAL *****
        self.RED = "\033[1;31m"
        self.GREEN = "\033[0;32m"
        self.YELLOW = "\033[1;33m"
        self.RESET = "\033[0;0m"

        # ***** INICIANDO-A-FUNÇAO-PRINCIPAL *****
        self.mainSetup()

    def mainSetup(self):
        try:
            self.bannerInfo()
            self.nomeUsuario = input("[*] - Digite o seu nome:\n> ")
            self.dirProjecto = input(f"[*] - Digite a localizaçao do seu projecto {self.nomeUsuario}:\n> ")
            if len(argv) >= 2:
                for modulo in range(1, len(argv)):
                    self.moduloExportar = argv[modulo]
                    if "Linux" in self.sistema_operativo:
                        self.linuxSetup()
                    elif "Windows" in self.sistema_operativo:
                        self.windowSetup()
            else:
                while True:
                    print(f"""
    ****************************** COMANDOS ******************************
    * > a - [RETORNA AS INSTRUÇOES DO PROGRAMA] 
    * > i - [INICIA A EXPORTAÇAO DO MODULO]     
    * > s - [TERMINA O PROGRAMA]                
    **********************************************************************""")
                    init = input(f'[*] - Digite o comando a executar {self.nomeUsuario}:\n> ')
                    if init.lower() == 'a':
                        self.ajuda()
                    elif init.lower() == 'i':
                        self.moduloExportar = input("[*] - Digite o nome do modulo que deseja exportar:\n> ")
                        if "Linux" in self.sistema_operativo:
                            self.linuxSetup()
                        elif "Windows" in self.sistema_operativo:
                            self.windowSetup()
                    elif init.lower() == 's':
                        print(f"""{self.YELLOW}
    *********************************** BYEE ***********************************
    * [^_^] MUITO OBRIGADO PELO APOIO {self.nomeUsuario}, ATE BREVE..
    ****************************************************************************{self.RESET}""")
                        exit(0)
                    else:
                        print(f"""{self.YELLOW}
        *********************************** FALHA ***********************************
        * LAMENTO {self.nomeUsuario} COMANDO NAO DEFINIDO, TENTE NOVAMENTE..
        *****************************************************************************{self.RESET}""")
        except KeyboardInterrupt:
            print(f"""{self.YELLOW}
    *********************************** BYEE ***********************************
    * [^_^] MUITO OBRIGADO PELO APOIO {self.nomeUsuario}, ATE BREVE..
    ****************************************************************************{self.RESET}""")
            exit(0)

    def ajuda(self):
        print(fr"""{self.YELLOW}
    *********************************** AJUDA ***********************************
    * OLAA {self.nomeUsuario}
    * COM BASTANTE ORGULHO E UMA PITADA DE PRAZER
    * QUE APRESENTO-TE O EXPORTGC, UM SCRIPT-PROGRAMA COM A FINALIDADE
    * DE AUTOMATIZAR A EXPORTAÇAO DE PACOTES (MODULOS) DO NODEJS JA INSTALADOS
    * NA SUA MAQUINA PARA O SEU PROJECTO, TUDO ISSO OFFLINE!
    *
    * CHEGA DE BLA-BLA-BLA, VAMOS A INTRODUÇAO DOS COMANDOS E FUNCIONALIDADES:
    *
    * - COMO O PROPRIO NOME ELUCIDA, O EXPORTGC TEM A FUNCIONALIDADE PRINCIPAL
    *   DE EXPORTAR UM OU MAIS MODULOS DO SEU AMBIENTE VIRTUAL (GLOBAL) PARA O
    *   AMBIENTE VIRTUAL DO SEU PROJETO.
    * - O EXPORTGC PODE SER EXECUTADO PELO TERMINAL OU CLICANDO NO ICONE DO
    *   PROGRAMA..
    *
    *   EXECUÇAO PELO TERMINAL (Windows):
    *     C:\Users\nome_usuario> egc [nome_modulo] | egc [nome_dos_modulos_separados_por_espaços]
    *   EXECUÇAO PELO TERMINAL (Linux):
    *     nome_usuario@nome_maquina:/dir_qualquer$ egc [nome_modulo] | egc [nome_dos_modulos_separados_por_espaços]
    *
    * - APOS A EXECUÇAO O EXPORTGC PEDE PELA INTRODUÇAO DO NOME DO USUARIO (por cordialidade)
    *   LOCALIZAÇAO (completa) DO PROJETO DO USUARIO
    *   SE (o modulo nao for especificado), O NOME DO MODULO POR ESPORTAR
    *   A SEGUIR LOCALIZA O PACOTE DO MODULO NAS DUAS LOCALIZAÇOES MAIS ACESSADAS:
    *
    *   PASTA USUARIO COM SESSAO INICIADA - (npm list)
    *   GLOBAL NPM - (npm -g list)
    *
    *   CASO O PACOTE EXISTA, O COPIA PARA O SEU PROJECTO
    *   SE NAO, RETORNA A INFORMAÇAO E TERMINA O PROGRAMA
    *   SIMPLES E PRÁTICO..
    *
    *   (C) 2021 NURUL-GC
    *   (TM) ArtesGC Inc
    *
    *   Portfolio - https://github.com/ArtesGC/exportGC
    *   Website - https://artesgc.blog.home
    *****************************************************************************
    {self.RESET}""")

    def linuxSetup(self):
        """definiçoes para o linux"""
        if self.mle():
            try:
                print(f"""
    *********************************** INFO ***********************************
    * Exportando o modulo {self.GREEN}{self.moduloExportar}{self.RESET} do pacote do usuario local 
    * em: {self.GREEN}`{self.homeNpm()}{self.moduloExportar}`{self.RESET}
    ****************************************************************************""")
                sleep(0.5)
                print(copytree(f"{self.homeNpm()}{self.moduloExportar}", f"{self.dirProjecto}/node_modules/{self.moduloExportar}"))
                sleep(0.5)
                print(f"""{self.YELLOW}
    *********************************** BYEE ***********************************
    * Exportacao para {self.dirProjecto} bem sucedida {self.nomeUsuario}
    * Terminando o programa...
    ****************************************************************************{self.RESET}""")
            except Exception as erro:
                print(f"\n{self.RED}[ERRO] - {erro} - {self.DATAHORA}{self.RESET}")
            finally:
                exit(0)
        else:
            if self.mge():
                try:
                    print(f"""
    ****************************** INFO ******************************
    * Exportando o modulo {self.GREEN}{self.moduloExportar}{self.RESET} do pacote global
    * em: {self.GREEN}`{self.globalNpm()}{self.moduloExportar}`{self.RESET}
    ******************************************************************""")
                    sleep(0.5)
                    print(copytree(f"{self.globalNpm()}{self.moduloExportar}", f"{self.dirProjecto}/node_modules/{self.moduloExportar}"))
                    sleep(0.5)
                    print(f"""{self.YELLOW}
    ****************************** BYEE ******************************
    * Exportacao para {self.dirProjecto} bem sucedida {self.nomeUsuario}
    * Terminando o programa...
    ******************************************************************{self.RESET}""")
                except Exception as erro:
                    print(f"\n{self.RED}[ERRO] - {erro} - {self.DATAHORA}{self.RESET}")
                finally:
                    exit(0)
            else:
                print(f"""{self.RED}
    *********************************** FALHA ***********************************
    * [!!] - FALHA AO LOCALIZAR O MODULO {self.moduloExportar}
    *      PROVAVELMENTE AINDA NÃO O TENHA INSTALADO
    *      OU DIGITADO INCORRETAMENTE O SEU NOME..
    *****************************************************************************{self.RESET}""")
                exit(0)

    def windowSetup(self):
        """definiçoes para o windows"""
        if self.mle():
            try:
                print(f"""
    *********************************** INFO ***********************************
    * Exportando o modulo {self.GREEN}{self.moduloExportar}{self.RESET} do pacote do usuario local 
    * em: {self.GREEN}`{self.homeNpm()}{self.moduloExportar}`{self.RESET}
    ****************************************************************************""")
                sleep(0.5)
                print(copytree(f"{self.homeNpm()}{self.moduloExportar}", f"{self.dirProjecto}/node_modules/{self.moduloExportar}"))
                sleep(0.5)
                print(f"""{self.YELLOW}
    *********************************** BYEE ***********************************
    * Exportacao para {self.dirProjecto} bem sucedida {self.nomeUsuario}
    * Terminando o programa...
    ****************************************************************************{self.RESET}""")
            except Exception as _erro:
                print(f"\n{self.RED}[ERRO] - {_erro} - {self.DATAHORA}{self.RESET}")
            finally:
                exit(0)
        else:
            if self.mge():
                try:
                    print(f"""
    ****************************** INFO ******************************
    * Exportando o modulo {self.GREEN}{self.moduloExportar}{self.RESET} do pacote global
    * em: {self.GREEN}`{self.globalNpm()}{self.moduloExportar}`{self.RESET}
    ******************************************************************""")
                    sleep(0.5)
                    print(copytree(f"{self.globalNpm()}{self.moduloExportar}", f"{self.dirProjecto}/node_modules/{self.moduloExportar}"))
                    sleep(0.5)
                    print(f"""{self.YELLOW}
    ****************************** BYEE ******************************
    * Exportacao para {self.dirProjecto} bem sucedida {self.nomeUsuario}
    * Terminando o programa...
    ******************************************************************{self.RESET}""")
                except Exception as _erro:
                    print(f"\n{self.RED}[ERRO] - {_erro} - {self.DATAHORA}{self.RESET}")
                finally:
                    exit(0)
            else:
                print(f"""{self.RED}
    *********************************** FALHA ***********************************
    * [!!] - FALHA AO LOCALIZAR O MODULO {self.moduloExportar}
    *      PROVAVELMENTE AINDA NÃO O TENHA INSTALADO
    *      OU DIGITADO INCORRETAMENTE O SEU NOME..
    *****************************************************************************{self.RESET}""")
                exit(0)

    def bannerInfo(self):
        print(f"""{self.YELLOW}
        *********************************
        *           EXPORT-GC           *
        *      (PyDev + NodeJsDev)      *
        *********************************
        *  UMA SOLUÇAO PARA EXPORTAÇAO  *
        * DOS MODULOS DO NODEJS OFFLINE *
        *********************************
        {self.RESET}""")

    def mge(self):
        """modulo global existe?"""
        if "Linux" in self.sistema_operativo:
            return path.exists(f'{self.globalNpm()}{self.moduloExportar}')
        elif "Windows" in self.sistema_operativo:
            return path.exists(f'{self.globalNpm()}{self.moduloExportar}')

    def mle(self):
        """modulo local existe?"""
        if "Linux" in self.sistema_operativo:
            return path.exists(f'{self.homeNpm()}{self.moduloExportar}')
        elif "Windows" in self.sistema_operativo:
            return path.exists(f'{self.homeNpm()}{self.moduloExportar}')

    def homeNpm(self):
        """retorna a localizacao para a pasta local do node package manager
        no directorio local do usuario"""
        if "Linux" in self.sistema_operativo:
            return f"{getoutput('echo $HOME')}/node_modules/"
        elif "Windows" in self.sistema_operativo:
            return f"{getoutput('echo %HOMEPATH%')}/node_modules/"

    def globalNpm(self):
        """retorna a localizacao da pasta global do node package manager"""
        if "Linux" in self.sistema_operativo:
            return "/usr/lib/node_modules/"
        elif "Windows" in self.sistema_operativo:
            return f"{getoutput('echo %APPDATA%')}/npm/node_modules/"


if __name__ == '__main__':
    gcApp = EGC()
    try:
        if "Linux" in gcApp.sistema_operativo:
            # ***** ADD-TO-PATH *****
            system(f"export PATH='{path.abspath(curdir)}:$PATH'>>$HOME/.bashrc")
        elif "Windows" in gcApp.sistema_operativo:
            # ***** ADD-TO-GLOBAL-VAR *****
            system(f"export EXPORTGC={path.abspath(curdir)}")
    except Exception:
        print("\tFALHA AO ADICIONAR A LOCALIZAÇAO DO PROGRAMA AS VARIAVEIS GLOBAIS!\n\tPARA O EXECUTAR PELO TERMINAL POR FAVOR ADICIONE A PASTA ATUAL AS VARIAVEIS GLOBAIS MANUALMENTE..")
