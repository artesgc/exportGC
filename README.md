# ExportGC - [PyDev ft NodeJsDev](/ "Nurul-GC")

![intro](img/intro.png)

## Intro — Português

> Durante os meus estudos com o NodeJS
tive a necessidade de importar modulos que por ventura \
ja os tinha instalado (globalmente) na minha maquina. \
Que por algum motivo (estranho) nao os conseguia usar
nos meus projetos! \
Entao apercebi-me que os projetos do `node` importam os seus modulos
do ambiente virtual criado localmente pelo `npm`
na pasta `node_modules`.. \
Então pensei: `“SE EU NAO TENHO INTERNET COMO FAREI PARA REFAZER A TRANSFERÊNCIA
DE PACOTES QUE EU JA TENHO INSTALADOS? NEM ME ARRISCO A TERMINAR OS ESTUDOS AGORA SO POR FALTA DE INTERNET 😥🤔…”`

Então nasceu o ***exportGC***
Um programa que cópia o pacote do modulo `desejado` localizado \
na pasta global `npm -g list` do node e o instala no ambiente virtual
do seu projeto.

## Intro - English

> I was studying NodeJS
and I got the necessity to import some modules \
that I already had installed (globally) on my machine. \
For some reason (weird) I could not use them in my projects. \
Then I realize that for some `node` projects I have to get my virtual environment set
before try to import some modules from `node_modules` path. \
So i tought: `"WHAT CAN I DO FOR EXPORT SOME PACKAGE THAT I ALREADY HAVE INSTALLED
IF I DON'T HAVE INTERNET CONNECTION? I WON'T STOP TO STUDY NOW BECAUSE SOME FAULT OF INTERNET 😥🤔...."`

Then ***exportGC*** was born
A program that copy the package of `wished` module \
from a global path `npm -g list` of node and install it on virtual environment
of your project.

## Demo

![help](img/ajuda.png)
![quit](img/sair.png)

## Erro

![erro_modulo_existente](img/erro_modulo_ja_existe.png)

---

&copy; 2021 [Nurul Carvalho](mailto:nuruldecarvalho@gmail.com) \
&trade; [ArtesGC Inc](https://artesgc.home.blog)
